# Word Count Alternate

This version makes use of a dataset (a distributed collection of items) created from textfile.txt.

MapReduce can then be done easily:

Via flatMap, we transform the dataset of lines to a dataset of words by spliting each line on whitespace.
Next, we combine both groupByKey and count to derive the occurrence of each word found in the file as a dataset of (String, Long) pairs.
Finally, we use collect to collect the results.