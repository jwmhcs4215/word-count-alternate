// via Datasets and group by key

import org.apache.spark.sql.SparkSession

val out = "output"
val spark = SparkSession.builder.appName("Simple Application").getOrCreate()
val textFile = spark.read.textFile("textfile.txt").cache()
val wordCounts = textFile.flatMap(line => line.split(" ")).groupByKey(identity).count()
wordCounts.collect()

wordCounts.saveAsTextFile(out)
spark.stop()
System.exit(0)
